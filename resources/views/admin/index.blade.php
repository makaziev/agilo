@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form-group">
                    Сортировать
                    <form action="{{ route('admin.index') }}" method="post" style="min-width: 700px; overflow: hidden">
                        {{ csrf_field() }}
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" @if(request()->has('sort_view')) checked @endif name="sort_view">
                            <label class="form-check-label" for="inlineCheckbox1">Просмотренные</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" @if(request()->has('sort_not_view')) checked @endif name="sort_not_view">
                            <label class="form-check-label" for="inlineCheckbox2">Непросмотренные</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" @if(request()->has('sort_close')) checked @endif name="sort_close">
                            <label class="form-check-label" for="inlineCheckbox3">Закрытые</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox4" @if(request()->has('sort_not_close')) checked @endif name="sort_not_close">
                            <label class="form-check-label" for="inlineCheckbox4">Незакрытые</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox5" @if(request()->has('sort_answer')) checked @endif name="sort_answer">
                            <label class="form-check-label" for="inlineCheckbox5">Ответили</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox6" @if(request()->has('sort_not_answer')) checked @endif name="sort_not_answer">
                            <label class="form-check-label" for="inlineCheckbox6">Не ответили</label>
                        </div>
                        <div style="float: right;">
                            <button type="submit" class="btn btn-primary btn-sm mb-2">Применить</button>
                        </div>
                    </form>
                </div>
                <div class="card">
                    <h2 class="card-header">Заявки</h2>
                    <ul class="list-group list-group-flush">

                        @foreach ($posts as $post)
                            <li class="list-group-item">
                                <a href="{{ route('admin.show', $post->id) }}">
                                    @if(!$post->viewed)
                                        <b>{{ $post->title }}</b>
                                    @else
                                        {{ $post->title }}
                                    @endif

                                    @if($post->answer === \App\Post::ANSWER_USER)
                                        <span class="badge badge-danger">новое сообщение</span>
                                    @endif

                                    @if($post->closed === \App\Post::CLOSED)
                                        <span class="badge badge-success">closed</span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
