<?php
use Carbon\Carbon;
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Заявки</div>

                    <div class="card-body">
                        {{--                    @if (session('status'))--}}
                        {{--                        <div class="alert alert-success" role="alert">--}}
                        {{--                            {{ session('status') }}--}}
                        {{--                        </div>--}}
                        {{--                    @endif--}}

                        {{--                    You are logged in!--}}
                        @if(Auth::user()->isAdmin())
                            <a class="btn btn-primary" href="{{ route('admin.index') }}" role="button">Список всех
                                заявок</a>
                        @else
                            @if($date_diff > 0)
                                <a class="btn btn-primary" href="{{ route('post.create') }}" role="button">Создать
                                    заявку</a>
                            @endif
                            <a class="btn btn-primary" href="{{ route('post.index') }}" role="button">Список моих
                                заявок</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
