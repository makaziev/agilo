@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Ваши заявки</div>
                    <ul class="list-group list-group-flush">
                        @foreach ($posts as $post)
                            <li class="list-group-item">
                                <a href="{{ route('post.show', $post->id) }}">
                                    {{ $post->title }}

                                    @if($post->answer === \App\Post::ANSWER_ADMIN)
                                        <span class="badge badge-danger">новое сообщение</span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
