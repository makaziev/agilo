@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h3 class="card-header">{{ $post->title }}</h3>

                    <ul class="list-group">
                        @foreach($post->messages as $message)
                            @if($message->user->role !== \App\User::ROLE_ADMIN)
                                <? $msgStyle = 'margin: 5px 0 5px 49%;:important; border-radius: .25rem;' ?>
                                <? $msgClass = 'list-group-item-success' ?>
                            @else
                                <? $msgStyle = 'border-radius: .25rem; margin: 2px 0 0 1%;' ?>
                                <? $msgClass = 'list-group-item-danger' ?>
                            @endif
                            <li class="list-group-item {{ $msgClass }} w-50" style="{{ $msgStyle }}">
                                {{ $message->message }}
                            </li>
                        @endforeach
                    </ul>
                    <form method="post" action="{{ route('post.message') }}" style="padding: 10px 10px 10px 10px">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1"></label>
                            <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                        </div>
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <button type="submit" class="btn btn-primary">Написать</button>
                        <a class="btn btn-danger" href="{{ route('post.close', $post->id) }}">Закрыть заявку</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
