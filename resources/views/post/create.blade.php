@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Создать заявку</div>

                    <form method="post" action="{{ route('post.store') }}" style="padding: 10px 10px 10px 10px">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Заголовок</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" name="title">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Текст</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="message"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
