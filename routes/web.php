<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Support\Facades\Auth;

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/admin', 'AdminController@index')->name('admin.index');

Route::post('/admin', 'AdminController@index')->name('admin.index');

Route::get('/admin/show/{id}', 'AdminController@show')->name('admin.show');

Route::post('/admin/message', 'AdminController@message')->name('admin.message');

Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post', 'PostController@index')->name('post.index');

Route::get('/post/create', 'PostController@create')->name('post.create');

Route::post('/post', 'PostController@store')->name('post.store');

Route::post('/post/message', 'PostController@message')->name('post.message');

Route::get('/post/show/{id}', 'PostController@show')->name('post.show');

Route::get('/post/close/{id}', 'PostController@close')->name('post.close');
