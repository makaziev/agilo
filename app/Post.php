<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    public const ANSWER_ADMIN = 2;
    public const ANSWER_USER = 1;
    public const ANSWER_NULL = 0;

    public const VIEWED_ADMIN = 1;

    public const CLOSED = 1;

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'user_id', 'viewed', 'closed', 'answer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'post_id');
    }
}
