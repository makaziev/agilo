<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mail extends Mailable
{
    use Queueable, SerializesModels;

    public $post_id;
    public $route;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($post_id, $route)
    {
        $this->post_id = $post_id;
        $this->route = $route;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail', [
            'post_id' => $this->post_id,
            'route' => $this->route,
        ]);
    }
}
