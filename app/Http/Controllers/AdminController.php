<?php

namespace App\Http\Controllers;

use App\Message;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('can:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arr = [];
        if ($request->has('sort_view')) {
            $arr[] = ['viewed', 1];
        }
        if ($request->has('sort_not_view')) {
            $arr[] = ['viewed', null];
        }
        if ($request->has('sort_close')) {
            $arr[] = ['closed', 1];
        }
        if ($request->has('sort_not_close')) {
            $arr[] = ['closed', null];
        }
        if ($request->has('sort_answer')) {
            $arr[] = ['answer', 2];
        }
        if ($request->has('sort_not_answer')) {
            $arr[] = ['answer', 0];
        }
        $posts = Post::where($arr)->orderBy('created_at', 'desc')->get();
        return view('admin.index', [
            'posts' => $posts,
            'postAttr' => new Post(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function message(Request $request)
    {
        Message::create($request->all());
        $post = Post::find($request->get('post_id'));
        $post->answer = Post::ANSWER_ADMIN;
        $post->save();
        Mail::to($post->user->email)->send(new \App\Mail\Mail($post->id, 'post.show'));

        return redirect()->route('admin.show', $request->get('post_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $post->viewed = Post::VIEWED_ADMIN;
        if ($post->answer !== Post::ANSWER_ADMIN) {
            $post->answer = Post::ANSWER_NULL;
        }
        $post->save();

        return view('admin.show', [
            'post' => $post,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
