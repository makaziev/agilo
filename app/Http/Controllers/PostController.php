<?php

namespace App\Http\Controllers;

use App\Message;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $posts = Post::where('user_id', Auth::id())->where('closed', null)->orderBy('created_at', 'desc')
            ->get();
//        dd($posts);
        return view('post.index', [
            'posts' => $posts,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $post = new Post();
        $message = new Message();
        $post->title = $request['title'];
        $post->user_id = Auth::user()->id;
        $post->save();

        $message->message = $request['message'];
        $message->post_id = $post->id;
        $message->user_id = Auth::user()->id;
        $message->save();
        return redirect(route('post.index'));
    }

    public function message(Request $request)
    {
        $admin = User::where('role', User::ROLE_ADMIN)->first();
        $id = $request->get('post_id');
        Message::create($request->all());
        $post = Post::find($id);
        $post->answer = Post::ANSWER_USER;
        $post->save();
        Mail::to($admin->email)->send(new \App\Mail\Mail($post->id, 'admin.show'));

        return redirect()->route('post.show', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if ($post->answer !== Post::ANSWER_USER) {
            $post->answer = Post::ANSWER_NULL;
        }
        $post->save();

        return view('post.show', [
            'post' => $post,
        ]);
    }

    public function close($id)
    {
        $post = Post::find($id);
        $post->closed = Post::CLOSED;
        $post->save();

        return redirect()->route('post.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Post  $post
     * @return Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
