<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            return view('home', [
            ]);
        }
        else {
            $post = Post::where('user_id', Auth::id())->orderBy('created_at', 'desc')->first();
            $formatted_dt1 = Carbon::parse(Carbon::now());
            $formatted_dt2 = Carbon::parse($post->created_at);
            $date_diff = $formatted_dt1->diffInDays($formatted_dt2);

            return view('home', [
                'post' => $post,
                'date_diff' => $date_diff,
            ]);
        }
    }
}
